N = 110;
density = 0.01:0.01:1;
eps = 0.001;
scale = 110;

i_count = zeros(1,100);
for d_option=1:100
    D = scale*diag(ones(1,N));
    A = (sprand(N,N,density(d_option))-0.5)*2;
    A(A==-1)=0;
    A = A+D;
    
    sol_ec=ones(N,1);
    b=A*sol_ec;
    
    L = tril(A,-1);
    D=diag(diag(A));
    U = triu(A,1);
    S = -(L+D)\U;
    C = (L+D)\b;
    
    sol=zeros(N,10);
    
    err(1)=1;
    i=1;
    
    while err(i)>eps
        i=i+1;
        sol(:,i) = S*sol(:,i-1)+C;
        err(i) = max(abs((A*sol(:,i)-b)));
        if i>100000 break; end
    end
    i_count(d_option) = i;
end

scatter(density,i_count,50,"filled");
fontsize(gca,16,"points")
xlim([0 1]);
ylim([0 inf]);
axis square